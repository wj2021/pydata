import numpy as np
import matplotlib.pyplot as plt
import time

def score():
    score = np.array([[80, 89, 86, 67,79],
                      [78,97,89,67,81],
                      [90,94,78,67,74],
                      [91,91,90,67,69],
                      [76,87,75,67,86],
                      [94,92,93,67,64],
                      [86,85,83,67,80]])

    print(score)

    python_list = []

    for i in range(10000):
        python_list.append(i)
    # print(python_list)

    ndarray_list = np.array(python_list)

    # print(ndarray_list)

    # t1 = time.time()
    # a = sum(python_list)
    # t2 = time.time()
    # print("list time:\n", t2 - t1)
    #
    # t1 = time.time()
    # a = sum(ndarray_list)
    # t2 = time.time()
    # print("ndarray time:\n", t2 - t1)

    print(score.shape)
    print(score.ndim)
    print(score.data)
    print(score.dtype)
    print(score.itemsize)


def np_method():
    zeros = np.zeros((2,3,4))

    print(zeros)

    ones = np.ones((3,4))
    print(ones)

    eyes = np.eye(5)
    print(eyes)


def distribute():
    data = np.random.uniform(-1,1,size=10000)
    print(data)



    data2 = np.random.normal(0,10,1000000)
    print(data2)
    plt.figure(figsize=(20,8))

    plt.hist(data2,1000)

    plt.show()

def stock():
    stock_change = np.random.normal(loc=0, scale=1,size= (8, 10))
    print(stock_change.shape)
    # print(stock_change)
    #
    # new_data = stock_change.reshape(10,8)
    #
    # print(new_data.shape)
    # print(new_data)

    # print(stock_change.resize())

    stock_change.astype("int32")

    print(stock_change)

    temp = np.array([[1,2,3,4],[2,3,4,5],[3,4,5,6]])

    print(np.unique(temp))

    stock_change[stock_change > 0.5]  = 1
    print(stock_change)

    print(np.all(stock_change > 0))

    print(np.any(stock_change > 0))
    print(stock_change[0:2,0:2])

    print("======================================")
    temp = np.where( stock_change > 0, 2 , 0)
    print(temp)

    stat = np.array([[1, 2, 3, 4, 2, 3, 4, 5, 3, 4, 5, 6]])

    print(np.min(stat))
    print(np.max(stat))
    print(np.mean(stat))
    print(np.median(stat))
    print(np.std(stat))
    print(np.s(stat))

def score2():
    data1 = np.array([[80,86],
            [85,90],
            [90,95],
            [75,79]])

    data2 = np.array([[82,89],
            [85,90],
            [90,95],
            [75,79]])

    print(data1 + data2)

if __name__ == "__main__":
    score2()