from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import  TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.tree import export_graphviz

def nb_news():
    news = fetch_20newsgroups(subset="all")
    x_train,x_test, y_train,y_test = train_test_split(news.data, news.target)
    transfer = TfidfVectorizer()
    x_train = transfer.fit_transform(x_train)
    x_test = transfer.transform(x_test)

    estimator = MultinomialNB()
    estimator.fit(x_train, y_train)

    y_predict = estimator.predict(x_test)
    print("y_predict: \n",y_predict)
    print("real and predict:\n", y_test == y_predict)

    score = estimator.score(x_test, y_test)
    print("x_test\n",x_test)
    print("y_test\n",y_test)
    print("score:\n",score)

    return None

def decison_iris():
    iris = load_iris()

    x_train, x_test, y_train, y_test = train_test_split(iris.data, iris.target, random_state=22)

    estimator = DecisionTreeClassifier(criterion="entropy")
    estimator.fit(x_train, y_train)

    y_predict = estimator.predict(x_test)
    print("y-predict:\n", y_predict)
    print("直接对比真实值和预测值：\n", y_test == y_predict)

    score = estimator.score(x_test, y_test)
    print("score:\n", score)

    export_graphviz(estimator, out_file="iris_tree.dot",feature_names=iris.feature_names)


if __name__ == "__main__":
    decison_iris()