from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
import jieba

def datasets_demo():
    print("hi")
    iris =load_iris()
    print("Iris 数据集：\n",iris)
    print("Iris 数据集描述：\n",iris["DESCR"])
    print("Iris 特征值的名字：\n",iris.feature_names)
    print("Iris 特征值：\n",iris.data, iris.data.shape)

    x_train, x_test,y_train,y_test = train_test_split(iris.data,iris.target,test_size=0.2)

    print(x_train.shape)


def dict_demo():
    data = [{'city':'Beijing','temp':100},{'city':'Shuanghai','temp':90},{'city':'Guangzhou','temp':120}]

    transfer = DictVectorizer(sparse=False)
    data_new = transfer.fit_transform(data)

    print("data_new:\n", data_new)
    print("Feature Name: \n", transfer.get_feature_names_out())

def count_demo():
    data = ["life is short, i like like python","life is too long, i dislike python"]

    transfer = CountVectorizer()

    data_new = transfer.fit_transform(data)
    print("data_new:\n", data_new.toarray())
    print("Feature Name: \n", transfer.get_feature_names_out())


def count_ch_demo():
    data = ["一种还是一种今天很残酷，明天更残酷，后天很美好，但绝对大部分是死在明天晚上，所以每个人不要放弃今天。",
            "我们看到的从很远星系来的光是在几百万年之前发出的，这样当我们看到宇宙时，我们是在看它的过去。",
            "如果只用一种方式了解某样事物，你就不会真正了解它。了解事物真正含义的秘密取决于如何将其与我们所了解的事物相联系。"]
    data_new = []
    for sent in data:
        data_new.append(cut_word(sent))
    # print(data)
    # for w in data_new:
    #     print(list(w))
    transfer = CountVectorizer()

    data_final = transfer.fit_transform(data_new)
    print("data_new:\n", data_final.toarray())
    print("Feature Name: \n", transfer.get_feature_names_out())


def test_cut():
    s = "我爱北京天安门"
    # a = " ".join(list(cut_word(s)))
    a = list(cut_word(s))
    print(a)
    text = "我爱北京天安门"
    # text = "征战四海只为今日一胜，我不会再败了。"
    # jieba.cut直接得到generator形式的分词结果
    seg = jieba.cut(text)
    print(' '.join(seg))

    # 也可以使用jieba.lcut得到list的分词结果
    seg = jieba.lcut(text)
    print(seg)

def cut_word(text):
    result = " ".join(jieba.cut(text))
    return result

def tfidf_demo():
    data = ["一种还是一种今天很残酷，明天更残酷，后天很美好，但绝对大部分是死在明天晚上，所以每个人不要放弃今天。",
            "我们看到的从很远星系来的光是在几百万年之前发出的，这样当我们看到宇宙时，我们是在看它的过去。",
            "如果只用一种方式了解某样事物，你就不会真正了解它。了解事物真正含义的秘密取决于如何将其与我们所了解的事物相联系。"]
    data_new = []
    for sent in data:
        data_new.append(cut_word(sent))
    # print(data)
    # for w in data_new:
    #     print(list(w))
    transfer = TfidfVectorizer(stop_words=["一种","所以"])

    data_final = transfer.fit_transform(data_new)
    print("data_new:\n", data_final.toarray())
    print("Feature Name: \n", transfer.get_feature_names_out())


if __name__ == "__main__":
    # datasets_demo()
    # dict_demo()
    # count_demo()
    # test_cut()
    # count_ch_demo()
    tfidf_demo()