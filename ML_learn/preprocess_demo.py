import pandas as pd

from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler
from sklearn.feature_selection import VarianceThreshold
from scipy.stats import pearsonr
from sklearn.decomposition import PCA

def minmax_demo():
    data = pd.read_csv("dating.txt")
    data = data.iloc[:,:3]

    transfer = MinMaxScaler()
    data_new = transfer.fit_transform(data)

    print(data_new)
    return None

def stand_demo():
    data = pd.read_csv("dating.txt")
    data = data.iloc[:,:3]

    transfer = StandardScaler()
    data_new = transfer.fit_transform(data)

    print(data_new)
    return None


def variance_demo():
    data = pd.read_csv("factor_returns.csv")
    data = data.iloc[:,1:-2]
    # print("data:\n", data)

    transfer = VarianceThreshold(10)
    data_new = transfer.fit_transform(data)

    print("new data:\n", data_new.shape)

    return None


def pearsonr_demo():
    data = pd.read_csv("factor_returns.csv")
    data = data.iloc[:,1:-2]
    # print("data:\n", data)

    r1 = pearsonr(data["pe_ratio"], data["pb_ratio"])
    r2 = pearsonr(data["revenue"], data["total_expense"])
    print(r1,r2)

    return None

def PCA_demo():
    data = [[2, 8, 4, 5], [6, 3, 0, 8], [5, 4, 9, 1]]
    # transfer = PCA(n_components=2)
    transfer = PCA(n_components=0.9)

    data_new = transfer.fit_transform(data)
    print(data_new)
    return None

if __name__ == "__main__":
    # minmax_demo()
    # stand_demo()
    # variance_demo()
    # pearsonr_demo()
    PCA_demo()