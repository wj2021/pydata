from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import GridSearchCV

def knn_demo():
    iris = load_iris()
    x_train, x_test, y_train, y_test = train_test_split(iris.data, iris.target, random_state=6)

    transfer =StandardScaler()

    x_train = transfer.fit_transform(x_train)
    x_test = transfer.transform(x_test)
    estimator = KNeighborsClassifier()
    param_dict = {"n_neighbors":[1,3,5,7,9,11]}
    estimator = GridSearchCV(estimator,param_dict, cv=4)

    estimator.fit(x_train,y_train)

    y_predict = estimator.predict(x_test)
    print("y_predict: \n",y_predict)
    print("real and predict:\n", y_test == y_predict)

    score = estimator.score(x_test, y_test)
    print("x_test\n",x_test)
    print("y_test\n",y_test)
    print("score:\n",score)

    print("===================")
    print("最佳参数：\n",estimator.best_params_)
    print("最佳分数：\n",estimator.best_score_)
    print("最佳评估器：\n",estimator.best_estimator_)
    print("交叉验证结果：\n",estimator.cv_results_)

    return None


if __name__ == "__main__":
    knn_demo()