from sklearn.datasets import load_boston
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import SGDRegressor
from sklearn.metrics import mean_squared_error
from sklearn.linear_model import Ridge
import joblib

def linear1():
    boston = load_boston()

    x_train, x_test, y_train, y_test = train_test_split(boston.data, boston.target, random_state=22)

    transfer = StandardScaler()
    x_train = transfer.fit_transform(x_train)
    x_test = transfer.transform(x_test)

    # 4) 预估器
    estimator = LinearRegression()
    estimator.fit(x_train, y_train)

    print("正规方程，权重系数：\n", estimator.coef_)
    print("正规方程，偏置：\n", estimator.intercept_)

    y_predict = estimator.predict(x_test)
    print("预测房价：\n",y_predict)
    error = mean_squared_error(y_test,y_predict)
    print("正规方程, MSE：\n",error)

def linear2():
    boston = load_boston()

    x_train, x_test, y_train, y_test = train_test_split(boston.data, boston.target, random_state=22)

    transfer = StandardScaler()
    x_train = transfer.fit_transform(x_train)
    x_test = transfer.transform(x_test)

    estimator = SGDRegressor()
    estimator.fit(x_train, y_train)

    print("梯度，权重系数：\n", estimator.coef_)
    print("梯度，偏置：\n", estimator.intercept_)

    y_predict = estimator.predict(x_test)
    print("预测房价：\n",y_predict)
    error = mean_squared_error(y_test,y_predict)
    print("梯度, MSE：\n",error)

def linear3():
    boston = load_boston()

    x_train, x_test, y_train, y_test = train_test_split(boston.data, boston.target, random_state=22)

    transfer = StandardScaler()
    x_train = transfer.fit_transform(x_train)
    x_test = transfer.transform(x_test)

    estimator = Ridge()
    estimator.fit(x_train, y_train)

    print("Ridge，权重系数：\n", estimator.coef_)
    print("Ridge，偏置：\n", estimator.intercept_)

    y_predict = estimator.predict(x_test)
    print("预测房价：\n",y_predict)
    error = mean_squared_error(y_test,y_predict)
    print("梯度, MSE：\n",error)

    joblib.dump(estimator, "linear3.kpi")


if __name__ == "__main__":
    # linear1()
    # linear2()
    linear3()