import pandas as pd

def titanic():
    path = "./titanic.txt"

    titanic = pd.read_csv(path)

    # print(titanic)
    x = titanic["PClass","Age", "Sex"]
    y = titanic["Survived"]

    print(x,y)

if __name__ == "__main__":
    titanic()