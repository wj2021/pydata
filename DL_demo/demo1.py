import os
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"] = ""
from keras.datasets import mnist
from keras import models
from keras import layers
from tensorflow.keras.utils import to_categorical

def demo1():

    (train_images, train_labels), (test_images, test_labels) = mnist.load_data()

    network = models.Sequential()
    network.add(layers.Dense(512, activation='relu', input_shape=(28 * 28,)))
    network.add(layers.Dense(10, activation='softmax'))
    network.compile(optimizer='rmsprop',
                    loss='categorical_crossentropy',
                    metrics=['accuracy'])

    train_images = train_images.reshape((60000, 28 * 28))
    train_images = train_images.astype('float32') / 255

    test_images = test_images.reshape((10000, 28 * 28))
    test_images = test_images.astype('float32') / 255

    train_labels = to_categorical(train_labels)
    test_labels = to_categorical(test_labels)

    network.fit()

    test_loss, test_acc = network.evaluate(test_images, test_labels)
    print("test_acc:", test_acc)

def demo2():
    (train_images, train_labels), (test_images, test_labels) = mnist.load_data()
    print(train_images.ndim)
    print(train_images.shape)
    print(train_images.dtype)
    digit = train_images[4]
    import matplotlib.pyplot as plt
    plt.imshow(digit, cmap=plt.cm.binary)
    plt.show()

if __name__ == "__main__":
    demo2()