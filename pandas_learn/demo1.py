import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


def demo1():
   obj = pd.Series([4,7,-5,3])
   print(obj)

   data = {'state':['a','b','c','d'],
           'year':['2001','2002','2003','2004'],
           'pop':[1.5,1.9,1.1,1.6]}
   df = pd.DataFrame(data)
   print("Dataframe",df)
   return None


def demo2():
    stock_change = np.random.normal(0,1,(10,5))

    print(stock_change)

    stock_change = pd.DataFrame(stock_change)
    print(stock_change)


def demo3():
    stock_change = np.random.normal(loc=0, scale=1, size=(8, 10))
    print(stock_change)
    df = pd.DataFrame(stock_change)

    print(df[0][0])
    print("shape:\n",df.shape)
    print("columns:\n",df.columns)
    print("values:\n",df.values)
    print("==================================")
    print(df.head(2))
    print(df.tail(2))

def demo4():
    stock_change = np.random.normal(loc=0, scale=1, size=(8, 10))
    # print(stock_change)
    df = pd.DataFrame(stock_change)

    # print(df)
    #
    # print(df.describe())
    # print(df.max())
    df[1].plot()
    plt.show()
    pd.read_csv


if __name__ == "__main__":
    demo4()