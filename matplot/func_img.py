import matplotlib.pyplot as plt
import numpy as np
from matplotlib.patches import Ellipse, Circle


def liner(a,b):
    x = np.arange(-100, 100, 1)
    y = a*x + b;

    fig, ax = plt.subplots()
    # red dashes, blue squares and green triangles
    ax.axis([-100,100,-100,100])
    ax.plot(x,y)
    ax.axhline(0, color='red', linewidth=2)
    ax.axvline(0, color='red', linewidth=2)
    ax.grid()
    plt.show()

def quadratic(a,b,c):
    x = np.arange(-100, 100, 1)
    y1 = a*x*x + b*x + c
    y2 = a*x
    # red dashes, blue squares and green triangles
    plt.axis([-25,25,-25,25])
    plt.plot(x,y1)
    plt.plot(x,y2)
    plt.grid()
    plt.show()


def circle(r):
    x = y = np.arange(-4, 4, 0.1)
    x, y = np.meshgrid(x, y)
    plt.contour(x, y, x ** 2 + y ** 2, [9])  # x**2 + y**2 = 9 的圆形

    plt.axis('scaled')
    plt.show()

if __name__ == "__main__":
    liner(2,30)
    # liner(10,50)
    # quadratic(2,0,0)
    # circle(5)